<?php $this->load->view('main/header'); ?>

<div class="app-main__outer">
    <div class="app-main__inner">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div class="page-title-icon">
                        <i class="pe-7s-map fa fa-users ">
                        </i>
                    </div>
                    <div>Users
                        <div class="page-title-subheading">
                            Implement in your applications Google or vector maps.
                        </div> 
                    </div>
                </div>
            </div>
        </div>            
                        



        <div class="row">
            <div class="col-lg-12">
                    <div class="main-card mb-3 card">
                        <div class="card-body"><h5 class="card-title">Table striped</h5>
                            <table class="mb-0 table table-striped">
                                <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Group</th>
                                    <th><center>Action</center></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <th scope="row">1</th>
                                    <td>Mark</td>
                                    <td>Otto</td>
                                    <td>@mdo</td>
                                    <td width="200px" align="center">
                                        <button class="btn btn-primary" data-toggle="modal" data-target=".add-users-modal-lg"> <i class="fa fa-plus "></i> </button>
                                        <button class="btn btn-success" data-toggle="modal" data-target=".edit-users-modal-lg"> <i class="fa fa-pencil "></i> </button>
                                        <button class="btn btn-danger" data-toggle="modal" data-target="#delete-users"> <i class="fa fa-trash "></i> </button>
                                    </td>
                                </tr>
                                
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('main/footer'); ?>   

<!-- modal add-->

<div class="modal fade add-users-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">ADD NEW DATA</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="input-group">
                    <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-user "></i></span></div>
                    <input placeholder="input your name" type="text" class="form-control">
                </div><br>
                <div class="input-group">
                    <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-envelope "></i></span></div>
                    <input placeholder="input your email" type="email" class="form-control">
                </div><br>
                <div class="input-group">
                    <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-graduation-cap "></i></span></div>
                    <select name="select" id="exampleSelect" class="form-control">
                    <option value="">-- Choose your Authorized</option>
                    <option value="1">Administrator</option>
                    <option value="2">Member</option>
                </select>
                </div><br>
                <div class="input-group">
                    <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-lock "></i></span></div>
                    <input placeholder="input your password" type="password" class="form-control">
                </div><br>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>

<!-- modal edit-->

<div class="modal fade edit-users-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">UPDATE DATA</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="input-group">
                    <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-user "></i></span></div>
                    <input placeholder="input your name" type="text" class="form-control">
                </div><br>
                <div class="input-group">
                    <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-envelope "></i></span></div>
                    <input placeholder="input your email" type="email" class="form-control">
                </div><br>
                <div class="input-group">
                    <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-graduation-cap "></i></span></div>
                    <select name="select" id="exampleSelect" class="form-control">
                    <option value="">-- Choose your Authorized</option>
                    <option value="1">Administrator</option>
                    <option value="2">Member</option>
                </select>
                </div><br>
                <div class="input-group">
                    <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-lock "></i></span></div>
                    <input placeholder="input your password" type="password" class="form-control">
                </div><br>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Delete-->
<div class="modal fade" id="delete-users" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <h4> Are you sure want delete this data ? </h4>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                <button type="button" class="btn btn-danger">Yes, Delete it</button>
            </div>
        </div>
    </div>
</div>


