<?php $this->load->view('main/header'); ?>

<div class="app-main__outer">
    <div class="app-main__inner">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div class="page-title-icon">
                        <i class="pe-7s-map fa fa-pencil ">
                        </i>
                    </div>
                    <div>Update Data Maps
                        <div class="page-title-subheading">
                        </div> 
                    </div>
                </div>
            </div>
        </div>   

         <form action="<?php echo base_url(). 'maps/Overview/update'; ?>" method="post"> 
        <div class="row">
            <div class="col-lg-8">
                    <div class="main-card mb-3 card">
                        <div class="card-body">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-home "></i></span></div>
                                <input placeholder="input your name place" type="hidden" name="id" value="<?= $maps[0]->id; ?>" class="form-control" required>
                                <input placeholder="input your name place" type="text" name="place" value="<?= $maps[0]->place_name; ?>" class="form-control">
                            </div><br>
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-plus "></i></span></div>
                                <input placeholder="input your latitude" type="number" name="latitude" value="<?= $maps[0]->latitude; ?>" class="form-control" required>
                            </div><br>
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-minus "></i></span></div>
                                <input placeholder="input your longtitude" type="number" name="longtitude" value="<?= $maps[0]->longtitude; ?>" class="form-control" required>
                            </div><br>  
                            <a href="<?= base_url('maps/overview/index/'); ?>" class="btn btn-warning"><i class="fa fa-home "> Back to menu</i></a>
                            <button type="submit" class="btn btn-primary">Save changes</button> 
                   </div>
                </div>
            </div>
        </div>
        </form>

    </div>
</div>
<?php $this->load->view('main/footer'); ?> 