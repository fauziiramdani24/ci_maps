<?php $this->load->view('main/header'); ?>

<div class="app-main__outer">
    <div class="app-main__inner">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div class="page-title-icon">
                        <i class="pe-7s-map fa fa-globe ">
                        </i>
                    </div>
                    <div>Maps
                        <div class="page-title-subheading">
                            Implement in your applications vector maps.
                        </div> 
                    </div>
                </div>
            </div>
        </div>            
                        
        <div class="row">
            <div class="col-lg-12">
                    <div class="main-card mb-3 card">
                        <div class="card-body">
                        <button class="btn btn-primary" data-toggle="modal" data-target=".add-users-modal-lg"> <i class="fa fa-plus "></i> Tambah Data</button><br><br>
                            <table class="mb-0 table table-striped">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name Place</th>
                                    <th>Langitude</th>
                                    <th>Longitude</th>
                                    <th><center>Action</center></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if($maps) { ?>
                                <?php foreach ($maps as $result) { ?>
                                <tr>
                                    <th scope="row"><?= $result->id ?></th>
                                    <td><?= $result->place_name ?></td>
                                    <td><?= $result->latitude ?></td>
                                    <td><?= $result->longtitude ?></td>
                                    <td width="200px" align="center">
                                        <a href="<?= base_url('maps/overview/index_update/'.$result->id); ?>" class="btn btn-success"><i class="fa fa-pencil "></i></a>
                                        <!-- <button class="btn btn-danger" data-toggle="modal" data-target="#delete-users"> <i class="fa fa-trash "></i> </button> -->

                                        <button class="btn btn-danger" id="" onclick="alertDel(<?=$result->id?>)"> <i class="fa fa-trash "></i> </button>
                                    </td>
                                </tr>
                                <?php } ?>
                                <?php } else { ?>
                                    <td scope="row" align="center" colspan="5">Data not found</td>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('main/footer'); ?>   

<!-- modal add-->

<div class="modal fade add-users-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
<form action="<?php echo base_url(). 'maps/Overview/insert'; ?>" method="post">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
        <form action="<?php echo base_url(). 'maps/Overview/insert'; ?>" method="post">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">ADD NEW DATA</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="input-group">
                    <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-home "></i></span></div>
                    <input placeholder="input your name place" type="text" name="place" class="form-control">
                </div><br>
                <div class="input-group">
                    <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-plus "></i></span></div>
                    <input placeholder="input your latitude" type="text" name="latitude" class="form-control">
                </div><br>
                <div class="input-group">
                    <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-minus "></i></span></div>
                    <input placeholder="input your longtitude" type="text" name="longtitude" class="form-control">
                </div><br>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
        </form>
        </div>
    </div>

</div>


<!-- Modal Delete-->
<div class="modal fade" id="delete-users" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <h4> Are you sure want delete this data ? </h4>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                
                <a type="button" href="<?= base_url('maps/overview/delete/2'); ?>" class="btn btn-danger">Yes, Delete it</a>
            </div>
        </div>
    </div>
</div>

<script>


    function alertDel(idx) {
        var r = confirm("Are you sure want delete this data ?");
        if (r == true) {
            location.href = "<?= base_url('maps/overview/delete/'); ?>"+idx;
        } else {
            txt = "You pressed Cancel!";
        }
	}

</script>


