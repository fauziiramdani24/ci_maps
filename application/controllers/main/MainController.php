<?php

class MainController extends CI_Controller {
    public function __construct()
    {
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('m_maps');
	}

	public function index()
	{
		$data['maps'] = $this->m_maps->tampil_data()->result();
        $this->load->view("main/index", $data);
	}
}

?>
