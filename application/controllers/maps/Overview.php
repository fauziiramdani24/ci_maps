<?php

class Overview extends CI_Controller {
    public function __construct()
    {
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('m_maps');
	}

	public function index()
	{
		// load view admin/overview.php
		$data['maps'] = $this->m_maps->tampil_data()->result();
		// print_r($data);die();
        $this->load->view("maps/index", $data);
	}

	public function index_update($id)
	{
		$data['maps'] = $this->m_maps->edit_data($id)->result();
		//print_r($data);die();
        $this->load->view("maps/update", $data);
	}

	public function insert() 
	{
		$place = $this->input->post('place');
		$latitude = $this->input->post('latitude');
		$longtitude = $this->input->post('longtitude');
 
		$data = array(
			'place_name' => $place,
			'latitude' => $latitude,
			'longtitude' => $longtitude
			);
		
		$this->m_maps->input_data($data,'maps');
		redirect('maps/overview');
	}

	public function update() 
	{
		$id = $this->input->post('id');
		$place = $this->input->post('place');
		$latitude = $this->input->post('latitude');
		$longtitude = $this->input->post('longtitude');
 
		$data = array(
			'id' => $id,
			'place_name' => $place,
			'latitude' => $latitude,
			'longtitude' => $longtitude
			);
			//print_r($data);die();
		$this->m_maps->update_data($data,'maps');
		redirect('maps/overview');
	}

	function delete($id){
		$where = array('id' => $id);
		$this->m_maps->hapus_data($where,'maps');
		redirect('maps/overview');
	}


}

?>
