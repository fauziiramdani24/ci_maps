

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_maps extends CI_Model{

    
	function tampil_data(){
		return $this->db->get('maps');
    }
    
    function edit_data($id){
		return $this->db->get_where('maps', array('id' => $id));
	}
 
	function input_data($data,$table){
		$this->db->insert($table,$data);
    }
    
    function hapus_data($where,$table){
        $this->db->where($where);
        $this->db->delete($table);
    }

    function update_data($data,$table){
        $this->db->update($table,$data, array('id' => $data['id']));
        print($data->id);
    }

}
?>